import React from 'react'
// @ts-ignore
import {BrowserRouter, Redirect} from 'react-router-dom'
// @ts-ignore
import { Route, Switch } from 'react-router-dom';
import './style.css'
import NewsList from "../NewsList";
import NewsPage from "../NewsPage";


class Layout extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <header>Simple News</header>
                <div className="content">
                    <Switch>
                        <Route exact path='/' component={NewsList}/>
                        <Route exact path='/story' render={({location}) => (
                                location.state ? <NewsPage location={location}/> : <Redirect to='/' />
                            )}/>
                        <Route render={() => <Redirect to='/' />} />
                    </Switch>
                </div>
                <footer>* data taken from https://hacker-news.firebaseio.com/</footer>
            </BrowserRouter>
        );
    }
}

export default Layout;