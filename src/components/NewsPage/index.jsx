import React from 'react'
import { Link } from 'react-router-dom'

class NewsPage extends React.Component {

    render() {
        const {title, url} = this.props.location.state
        return (
            <div>
                <div><a href={url}>{title}</a></div>
                <Link to="/">back</Link>
            </div>
        );
    }
}

export default NewsPage;