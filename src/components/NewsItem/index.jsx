import React from 'react'
import axios from 'axios';

import { Link } from 'react-router-dom'

import loader from '../../three-dots.svg'

const CancelToken = axios.CancelToken
const source = CancelToken.source()

const BASE_API = 'https://hacker-news.firebaseio.com/v0/item/'


class NewsItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            item: false,
            loaded : false
        }

        this.fetchStory = this.fetchStory.bind(this)
    }

    componentDidMount() {
        this.fetchStory(this.props.storyId)
    }

    fetchStory(id) {
        axios.get(BASE_API + id + '.json',{cancelToken: source.token})
            .then(data => {
                this.setState({
                    item: data.data
                })
            })
            .catch( (error) => {
                if (axios.isCancel(error)) {
                    console.log('Request canceled', error.message);
                } else {
                    console.log('pickels=> error', error.message)
                }
            })
    }
    componentWillUnmount() {
        source.cancel('Operation canceled by navigation.');
    }

    render() {
        const {item} = this.state
        return (
            <li>
                { item
                    ? <Link
                        to={{
                            pathname:"/story",
                            state: item
                        }}
                    >
                        <div className="title">
                            {item.title}
                        </div>
                    </Link>
                    : <img src={loader} alt="loader"/>
                }
            </li>
        );
    }
}

export default NewsItem;