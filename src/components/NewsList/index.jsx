import React from 'react';
import axios from 'axios';

import './style.css'
import NewsItem from "../NewsItem"

const API = 'https://hacker-news.firebaseio.com/v0/topstories.json'


class NewsList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            list: []
        }

        this.fetchList()
    }

    fetchList() {
        axios.get(API)
            .then(data => {
                this.setState({
                    list:data.data
                })
                console.log('pickels=> ',data)
            })
            .catch(error => console.log('pickels=> error',error))
    }

    render() {
        const {list} = this.state
        return (
            <div>
                <ul className="infoList">
                    {list.map( (item, index) => {
                        return (
                            <NewsItem key={index} storyId={item}/>
                        )
                    })}
                </ul>
            </div>
        );
    }
}

export default NewsList;