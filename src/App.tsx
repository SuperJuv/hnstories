import React from 'react';
import './App.css';
import Layout from "./components/Layout/index";

const App: React.FC = () => {
  return (
    <div className="App">
      <Layout/>
    </div>
  );
}

export default App;
